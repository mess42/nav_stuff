Place your .gpx files in this folder.

If the search provider "GPX" is chosen in the settings,
all files will be read on startup and the content is stored in RAM.

If you have many large .gpx files, this can cause performance issues.

Only objects with a <name> or <comment> tag can be searched.

As a .gpx file can contain several points, tracks and routes,
the <gpx><metadata><name> or the filename cannot be searched.

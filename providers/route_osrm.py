#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This file defines route provider.
A route provider finds a set of roads, leading from a start location to a destination.
"""

import numpy as np

from polyline.polyline.codec import PolylineCodec

import helpers.download
import helpers.angles

from providers.route_baseclass import Router

class OSRM(Router):
    def set_route(self, waypoints):
 
        waypoints_as_str = ";".join( list(",".join(point) for point in np.array(waypoints, dtype=str) ) )
        url = self.url_template.replace("{waypoints}", waypoints_as_str)
        d = helpers.download.remote_json_to_py(url=url)
        
        if d["code"].upper() != "OK":
            raise Exception(d["code"])
        
        self.route = d["routes"][0]
        self.__set_maneuvers(route = self.route)
        
        
    def __set_maneuvers(self, route):    
        """
        @brief read the OSRM routing information and wrap it to a usable format.
        
        @param route (dict)
            dictionary typecast from the OSRM routing json.
        
        route
         ├ weight name
         ├ weight
         ├ distance
         ├ duration
         └ legs (list)
            └ leg[0]
               ├ weight
               ├ distance
               ├ summary
               ├ duration
               └ steps (list)
                 └ step[0]
                   ├ intersections (list)
                   │ ├ intersection[0]  : maneuver intersection
                   │ └ intersection[1:] : all intersections after this maneuver, but before the next maneuever
                   │    ├ bearings : angles of all roads in this intersection (0 is north)
                   │    └ location : latitude and longitude of the intersection
                   ├ duration
                   ├ driving side: left or right hand driving
                   ├ name : name of the road after the maneuver
                   ├ weight
                   ├ mode
                   └ maneuver
                      ├ type     : basic type of the maneuver, like "turn", "on ramp" or "fork"
                      ├ modifier : optional
                      └ exit     : roundabout only: number of exit
        """

        # First shot: every OSRM step is exactly one maneuver.
        # TODO: Rotary entry and exit are 2 separate steps. 
        #       Both, entry and exit, are represented by a 3-way crossing (rotary, rotary, exit).
        #       Better: combine (all intersections of the entry step) and (the first intersection of the exit step) 
        #       to one big icon.
        # TODO: Notifications are a maneuver. Let's check whether this is a good choice in practice.
        # TODO: use different icon types for roundabouts
        # TODO: use different icon type for U-Turn

        self.lat_deg   = [] # polygon line of the whole route
        self.lon_deg   = []
        self.maneuvers = [] # information about maneuvers
        
        icon_types = {"arrive":"arrive",
           "continue"        :"crossing",
           "straight"        :"crossing",
           "depart"          :"nesw_arrow",
           "end of road"     :"crossing",
           "exit rotary"     :"crossing",
           "new name"        :"crossing",
           "turn"            :"crossing",
           "fork"            :"crossing",
           "merge"           :"crossing",
           "on ramp"         :"crossing",
           "off ramp"        :"crossing",
           "roundabout"      :"crossing",
           "rotary"          :"crossing",
           "notification"    :"notification",
           "roundabout turn" :"crossing",
           "exit roundabout" :"crossing",
            }

        if "legs" in route:
            for leg in route["legs"]:
                for step in leg["steps"]:
                    maneuver_ind =  max( 0, len(self.lat_deg)-1 )

                    coords = np.array(step["geometry"]["coordinates"])
                    self.lat_deg = np.hstack([self.lat_deg, coords[:,1]])
                    self.lon_deg = np.hstack([self.lon_deg, coords[:,0]])
                       
                    typ = step["maneuver"]["type"]

                    modifier = ""
                    if "modifier" in step["maneuver"]:
                        modifier = step["maneuver"]["modifier"]
                        if modifier == "uturn":
                            typ = "uturn"
                        if typ == "turn" and modifier == "straight":
                            typ = "straight"
                
                    bearings_deg = step["intersections"][0]["bearings"]

                    in_bearing_deg = float("nan")
                    if "in" in step["intersections"][0]:
                        in_bearing_deg = bearings_deg[ step["intersections"][0]["in"] ]

                    out_bearing_deg = float("nan")
                    if "out" in step["intersections"][0]:
                        out_bearing_deg = bearings_deg[ step["intersections"][0]["out"] ]

                    exit_number = 0
                    if "exit" in step["maneuver"]:
                        exit_number = step["maneuver"]["exit"]
                    
                    self.maneuvers.append({
                       "location"            : step["intersections"][0]["location"],
                       "bearings_deg"        : np.array(bearings_deg),
                       "in_bearing_deg"      : in_bearing_deg,
                       "out_bearing_deg"     : out_bearing_deg,
                       "icon_type"           : icon_types[ typ ],
                       "left_driving"        : ( step["driving_side"].upper() == "LEFT" ),
                       "type"                : typ,
                       "street_name_after"   : step["name"],
                       "movement_modifier"   : modifier,
                       "exit_number"         : exit_number,
                       "ind_on_route"        : maneuver_ind,
                       "distance_to_next"    : step["distance"],
                        })

    	# appending maneuvers finished,
	    # let's add some data of the whole route
	
        # calculate the road distance from the start
        self.dist_from_start = self.calc_dictances_from_start(lat_deg = self.lat_deg, lon_deg=self.lon_deg)
        
        # add distance to previous
        self.maneuvers[0]["distance_to_prev"] = 0
        for i_man in np.arange(len(self.maneuvers)-1)+1:
            self.maneuvers[i_man]["distance_to_prev"] = self.maneuvers[i_man-1]["distance_to_next"]

        # add whole route to each maneuver
        # (Python uses lazy copy, so RAM is occupied only once)
        for i_man in np.arange(len(self.maneuvers)):
            self.maneuvers[i_man]["route_lat_deg"]              = self.lat_deg
            self.maneuvers[i_man]["route_lon_deg"]              = self.lon_deg
            self.maneuvers[i_man]["distances_from_route_start"] = self.dist_from_start
    


class OSM_Scout(Router):    
    #TODO: put OSM scout code in its own file
    def set_route(self, waypoints):
        locs = []
        for point in np.array(waypoints):
            locs += ["{\"lat\": " + str(point[1]) + ", \"lon\": " + str(point[0]) +"}"]
        locations     = "\"locations\": [" + ", ".join(locs) + "]" 
        
        # TODO: no hard coded other_options, define them in profiles.json
        other_options = "\"costing\": \"auto\", \"costing_options\": {\"auto\": {\"use_ferry\": 0.5, \"use_highways\": 1, \"use_tolls\": 0.5}},  \"directions_options\": {\"language\": \"en\", \"units\": \"kilometers\"}"
        
        json_str = "{" + locations + ", " + other_options + "}"
        json_str = helpers.download.encode_special_characters( json_str )
        url = self.url_template.replace("{json}", json_str)

        d = helpers.download.remote_json_to_py(url=url)
        
        if d["trip"]["status"] != 0:
            raise Exception( d["trip"]["status_message"] )

        self.route = d["trip"]
        self.__set_maneuvers( route = self.route )

    def __set_maneuvers(self, route):    

        self.lat_deg = []
        self.lon_deg = []
        self.maneuvers = []
        
        polycodec = PolylineCodec()
        
        for leg in route["legs"]:
            coords = np.array( polycodec.decode( leg["shape"], precision=6) )

            self.lat_deg = np.hstack([self.lat_deg, coords[:,1]])
            self.lon_deg = np.hstack([self.lon_deg, coords[:,0]])
            
        # TODO: maneuvers



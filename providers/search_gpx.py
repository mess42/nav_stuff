#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  8 16:31:29 2021

@author: foerster
"""

import os
import gpxpy

import numpy as np

class GPXSearch( ):
    def __init__(self, gpx_folder):
        filenames = list( gpx_folder + "/" + f for f in os.listdir(gpx_folder) if f.endswith(".gpx") )
        
        self.gpx_objects = self.read_gpx_files(filenames)
        self.all_objects = self.get_all_objects( self.gpx_objects )
        
        self.search_dict = self.make_search_dict( all_objects = self.all_objects )
        
    
    def read_gpx_files(self,filenames):
        """
        @brief: Parse several .gpx files.
        
        @param filenames: list of str

        @return gpx_objects: list of gpxpy.gpx.GPX objects
        """
        gpx_objects = []
        for filename in filenames:
            with open( filename, "r" ) as f:
                gpx_objects.append( gpxpy.parse(f) )
        return gpx_objects
    
    
    def get_all_objects(self, gpx_objects):
        """
        @brief: Extract all child objects of a list of gpx objects.
        
        track, track segment, track point, waypoint, route, route point
        """
        all_objects = []
        for gpx in gpx_objects:
            
            for track in gpx.tracks:
                all_objects.append( track )
                for segment in track.segments:
                    all_objects.append( segment )
                    for point in segment.points:
                        all_objects.append( point )
            
            for waypoint in gpx.waypoints:
                all_objects.append(  waypoint )
            
            for route in gpx.routes:
                all_objects.append(  route )
                for point in route.points:
                    all_objects.append( point )
        return all_objects
            
    
    def make_search_dict(self, all_objects):
        d = {}
        
        for obj in all_objects:
            search_key = self.make_search_key( obj )
            
            if search_key:
                latlon = self.make_latlon( obj )
                value  = {"object": obj,
                          "display_name": self.make_display_name( obj ),
                          "lat": latlon[0],
                          "lon": latlon[1],
                          "relevance": self.get_relevance( obj )
                         }
                    
                d[search_key] = value
        
        return d

    
    def make_search_key(self, obj):
        search_key = ""
        if hasattr(obj, "name") and obj.name:
            search_key += obj.name
        search_key += " "
        if hasattr(obj, "comment") and obj.comment:
            search_key += obj.comment
        search_key = search_key.strip().lower()
        return search_key


    def make_display_name(self, obj):
        name  = str(   type(obj)   ).split("'")[1].split(".")[-1]
        name += " "
        name += self.make_search_key( obj )
        name = name.strip()
        return name


    def make_latlon(self, obj):
        """
        @brief : Get the latitude and longitude of an object.
        
        @param obj : gpx waypoint, track, segment, route or point
        
        @return latlon:
        """
        makers = {gpxpy.gpx.GPXTrack       : self.make_latlon_of_track,
                  gpxpy.gpx.GPXTrackSegment: self.make_latlon_of_track_segment,
                  gpxpy.gpx.GPXTrackPoint  : self.make_latlon_of_point,
                  gpxpy.gpx.GPXWaypoint    : self.make_latlon_of_point,
                  gpxpy.gpx.GPXRoute       : self.make_latlon_of_track_segment,
                  gpxpy.gpx.GPXRoutePoint  : self.make_latlon_of_point,
                 }
        
        maker  = makers[type(obj)]
        latlon = maker( obj )
        return latlon
        

    def make_latlon_of_track(self,track):
        track_lat = []
        track_lon = []
        for seg in track.segments:
            seglat, seglon  = self.make_latlon(seg)
            track_lat += seglat
            track_lon += seglon
        return track_lat, track_lon
        
        
    def make_latlon_of_track_segment(self, segment):
        latlon = list( self.make_latlon(p) for p in segment.points )
        lat    = list( float(l[0]) for l in latlon )
        lon    = list( float(l[1]) for l in latlon )
        return lat, lon


    def make_latlon_of_point(self, point):
        return [ float(point.latitude), float(point.longitude) ]

    
    def get_relevance( self, obj):
        """
        @brief : Get the relevance of an object to sort the results.
                 Used to sort search results.
                 A postprocessed route is more relevant than raw track data.
                 A track is more relevant than a single point.
        
        @param obj : gpx waypoint, track, segment, route or point
        
        @return relevance : float
        """
        relevances = {gpxpy.gpx.GPXTrack       : 0.9,
                      gpxpy.gpx.GPXTrackSegment: 0.8,
                      gpxpy.gpx.GPXTrackPoint  : 0.1,
                      gpxpy.gpx.GPXWaypoint    : 0.3,
                      gpxpy.gpx.GPXRoute       : 1.0,
                      gpxpy.gpx.GPXRoutePoint  : 0.2,
                     }
        
        relevance  = relevances[type(obj)]
        return relevance        


    def find(self,query):
        results        = list( self.search_dict[key] for key in self.search_dict if query.lower() in key)
        ind            = np.argsort( list( r["relevance"] for r in results ) )[::-1]
        sorted_results = list( results[ind[i]] for i in range(len(results)) )
        return sorted_results

           


if __name__ == "__main__":
    gpx_folder = "../my_gpx_tracks"

    searcher = GPXSearch( gpx_folder )

    res = searcher.find("London")
   


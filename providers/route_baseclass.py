#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This file defines route provider.
A route provider finds a set of roads, leading from a start location to a destination.
"""

import numpy as np
import helpers.angles

class Router(object):
    def __init__(self, url_template= "", waypoints=[]):
        """
        @brief: A router that connects start an destination by a series of roads and turns.
            This is a baseclass that provides all methods, but does not do anything.
            Use if you wish no routing.
        
        @param url_template (str)
            URL to catch the route from, either localhost or an online provider.
            All parameters (like vehicle type, shortest or fastest) should
            be hardcoded in the URL, except for waypoints.
            The place where waypoints should be inserted shall be marked by
            the string '{waypoints}'.
        @param waypoints (numpy array)
            If not empty, a trip will be generated.
            See self.set_route for details.
        """
        self.url_template = url_template
        self.waypoints = waypoints
        self.trip = {}
        if len(self.waypoints) != 0:
            self.route = self.set_route(waypoints)


    def get_polyline_of_whole_route(self, color_rgba = (0,0,1,0.5) ):
        return {"lat_deg":self.lat_deg,"lon_deg":self.lon_deg, "color_rgba": color_rgba}
    
    
    def calc_dictances_from_start(self, lat_deg, lon_deg):
        delta = helpers.angles.haversine_distance(lat1_deg = lat_deg[:-1], 
                                                  lon1_deg = lon_deg[:-1], 
                                                  lat2_deg = lat_deg[1:], 
                                                  lon2_deg = lon_deg[1:])
        dist_from_start = np.hstack( [[0], np.cumsum(delta)] )
        return dist_from_start



    def set_route(self, waypoints):
        """
        @brief: Calculate the route inbetween given waypoints.
        
        @param waypoints: (2d numpy array of Nx2 float)
             Longitudes and latitudes of 
             route start, intermediate destinations, and final destination.
             Intermediate points are optional. 
             Use intermediate points if you wish to go via a certain location.
             For round trips, first and last waypoint are identical,
             and at least one intermediate point is required.
             waypoints[:,0] are longitudes
             waypoints[:,1] are latitudes
             
        sets the following variables:
        self.lat_deg  : list or 1d numpy array of float
                        polygon line of the whole route
        self.lon_deg  : list or 1d numpy array of float
        self.dist_from_start : list or 1d numpy array of float
                        path length of the positions 
                        ( self.lat_deg, slef.lon_deg )
                        in meters
        self.maneuvers: list of dict
                        "location"            : list of 2 floats (lat, lon in deg)
                                                location of the intersection
                        "bearings_deg"        : 1d numpy array of floats
                                                angles of all roads of this intersection (0 is north)
                        "in_bearing_deg"      : float
                                                angle of the road on which we approach the maneuver location
                        "out_bearing_deg"     : float
                                                angle of the road on which we leave the maneuver loacation
                        "icon_type"           : str
                                                name of the maneuver icon to display
                        "left_driving"        : bool
                                                left hand driving (Britain, Japan)
                        "type"                : str
                                                maneuver type, like "turn", "on ramp" or "fork"
                        "street_name_after"   : str
                                                road name after the maneuver
                        "movement_modifier"   : str
                                                OSRM modifier; default is "" if this key is not in the input dict/json
                        "exit_number"         : int
                                                roundabout only
                        "ind_on_route"        : int
                                                index of the maneuver location in (self.lat_deg, self.lon_deg)
                                                TODO: is "location" key redundant ?
                        "distance_to_next"    : float
                                                path distance to the next maneuver in meter
                        "distance_to_prev"    : float
                                                path distance to the previous maneuver in meter
                        "route_lat_deg"       : lazy copy of self.lat_deg
                        "route_lon_deg"       : lazy copy of self.lon_deg
                        "distances_from_route_start" : lazy copy of self.dist_from_start
        """
        self.lat_deg = []
        self.lon_deg = []
        self.maneuvers = []
        self.dist_from_start = []
        

class RouterWithWaypointPolyline(Router):
    def set_route(self, waypoints):
        self.lat_deg = np.array(waypoints, dtype=float)[:,1]
        self.lon_deg = np.array(waypoints, dtype=float)[:,0]
        
        if len(self.lat_deg) < 3:
            # add first point at the beginning and make it size 3
            self.lat_deg = np.hstack([[self.lat_deg[0]], self.lat_deg])
            self.lon_deg = np.hstack([[self.lon_deg[0]], self.lon_deg])
        
        self.maneuvers = []
        self.dist_from_start = self.calc_dictances_from_start(lat_deg = self.lat_deg, lon_deg=self.lon_deg)

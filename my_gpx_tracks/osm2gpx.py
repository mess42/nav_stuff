#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This script downloads an OpenStreetMap Relation by its ID and converts it to GPX.
"""
import sys
import requests
import xml.dom.minidom as minidom
import gpxpy

import matplotlib.pyplot as plt


def get_remote_doc(url):
    """
    @brief Download a XML file and parse it.

    @param url : str
    @return doc : xml.dom.minidom Document object
    """
    response = requests.get(url)
    xml_str  = response.content.decode("utf-8")
    doc      = minidom.parseString( xml_str )
    return doc
    

def extract_data_from_relation( osm_tag ):
    """
    @brief Convert a <osm> tag minidom object to dicts.

    @param osm_tag : minidom Element
        parsed version of the <osm> tag 
        of a openstreetmap "relation" type object

    @return gpx_data : dict
        "waypoints" : list of dicts
            "lat": float
            "lon": float
        "tracks": dict
            "name" : str
            "segments": list of list of dicts
                "lat": float
                "lon": float
                
    "lat" and "lon" are latitude and longitude coordinates in degrees.
    Height information is missing.
    <node> members of the relation are converted to "waypoints".
    <way> members of the relation are converted to "segments"
    """
    nodes = {}
    for n in osm_tag.getElementsByTagName("node"):
        nodes[ n.getAttribute("id") ] = {
                "lat": n.getAttribute("lat"),
                "lon": n.getAttribute("lon")
                }
        
    ways = {}
    for w in osm_tag.getElementsByTagName("way"):
        ways[ w.getAttribute("id") ] = list( nodes[ nref.getAttribute("ref") ] for nref in w.getElementsByTagName("nd") )
    
    relation = osm_tag.getElementsByTagName("relation")[0]
    members  = relation.getElementsByTagName("member")

    way_refs  = list( m for m in members if m.getAttribute("type") == "way")
    node_refs = list( m for m in members if m.getAttribute("type") == "node")

    track_segs = list( ways[r.getAttribute("ref")]  for r in way_refs  )
    waypoints  = list( nodes[r.getAttribute("ref")] for r in node_refs )
    
    tags = {}
    for t in relation.getElementsByTagName("tag"):
        tags[ t.getAttribute("k") ] = t.getAttribute("v")
    
    track = {
             "name": tags["name"],
             "segments": track_segs
            }
    
    gpx_data = {
           "waypoints" :   waypoints,
           "tracks"    : [ track ]
          }
    
    return gpx_data


def write_gpx_file(filename, gpx_data):
    """
    @brief write the gpx_data dict to a .gpx file

    @param gpx_data : dict
    """
    gpx = gpxpy.gpx.GPX()
    
    for wp in gpx_data["waypoints"]:
        gpx.waypoints.append( gpxpy.gpx.GPXWaypoint(latitude = wp["lat"], longitude= wp["lon"] ))
    
    for t in gpx_data["tracks"]:
        track = gpxpy.gpx.GPXTrack( name = t["name"] )
        for s in t["segments"]:
            segment = gpxpy.gpx.GPXTrackSegment()
            for p in s:
                point = gpxpy.gpx.GPXTrackPoint(latitude=p["lat"], longitude=p["lon"])
                segment.points.append(point)
            track.segments.append( segment )
        gpx.tracks.append( track )
    
    with open(filename, "w") as f:
        f.write( gpx.to_xml() )

    

if __name__  == "__main__":
    relation_id = sys.argv[1]
    
    out_filename = "osm_relation_" + str(relation_id) + ".gpx"
    
    url      = f"https://www.openstreetmap.org/api/0.6/relation/{relation_id}/full"
    
    osm_tag  = get_remote_doc(url).getElementsByTagName("osm")[0]
    
    gpx_data = extract_data_from_relation( osm_tag = osm_tag )
    
    write_gpx_file( filename=out_filename, gpx_data=gpx_data )

    print("tracks written:")    
    for t in gpx_data["tracks"]:
        print("    " + t["name"])    

    """
    plt.figure()
    for t in gpx_data["tracks"]:
        plt.title( t["name"])
        print(t["name"])
        for s in t["segments"]:
            lats = list( float(p["lat"]) for p in s )
            lons = list( float(p["lon"]) for p in s )
            plt.plot( lons, lats )
            
    plt.xlabel("Longitude (deg)")
    plt.ylabel("Latitude (deg)")
    plt.show()
    """
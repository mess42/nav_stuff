#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This file defines route provider.
A route provider finds a set of roads, leading from a start location to a destination.
"""

import providers.route_baseclass
import providers.route_osrm


def get_mapping_of_names_to_classes():
    """
    @brief: Pointers to all classes that shall be usable.
    
    @return d (dict)
    """
    d = {"Router"   : providers.route_baseclass.Router,
         "RouterWithWaypointPolyline": providers.route_baseclass.RouterWithWaypointPolyline,
         "OSRM"     : providers.route_osrm.OSRM,
         "OSM_Scout": providers.route_osrm.OSM_Scout,
        }
    return d